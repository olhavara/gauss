### Zadanie (jedno z šuplíkových):
####Řešení soustav lin. rovnic
Další z velmi důležitých úloh lineární algebry. Aplikace na vstupu dostane rozšířenou matici soustavy a na výstup vypíše
vektor jejího řešení, případně zprávu o tom, že řešení neexistuje. Pokud je řešení nekonečně mnoho, program vypíše jedno 
partikulární řešení a bázi lineárního prostoru řešení přidružené homogenní soustavy. K implementaci je nejvýhodnější 
použít Gaussovu eliminaci se zpětným dosazením.


###Implementácia:
Po spustení programu je užívateľ vyzvaný na zadanie cesty k vstupnému súboru. Z neho sa načíta súbor úloh. Každá z úloh je 
daná číslom, ktoré označuje počet premenných a následne maticou sústavy. Po vyriešení úlohy alebo súboru úloh sú 
systém rovníc a množina riešení vypísané na výstup.

Rozhodol som sa pre variantu, ktorá príjma na vstupe len celočíselné koeficienty. Táto varianta neznižuje obecnosť 
úlohy, pretože systémy zadané číslami s desatinnou čiarkou alebo zlomkom majú racionálne koeficienty a tie sa dajú 
previesť prenásobením vhodnou konštantou na systém s celočíselnými koeficientami. Navyše, takéto zadanie umožňuje využiť 
heuristiky, ktoré sú založené na celočíselnosti. 

Riešenie prebieha Gaussovou elimináciou a následným spätným dosadením. Pre 
elimináciu sú použité riadky, ktoré sú z pohľadu rastu koeficientov najvýhodnejšie (také, použitím ktorých je nutné 
pri elimináciii prenásobiť nulované riadky čo najmenšími číslami). V priebehu eliminácie sa nepoužíva delenie, 
iba násobenie, čím je zachovaná celočíselnosť
až po dosiahnutie hornej trojuholníkovej matice. Prínosom je, že nemôžu vzniknúť "pseudo-nuly" t.j. koeficienty, ktoré
by mali byť nulové, ale nie sú kvôli nepresnostiam premenných typu double/float. Na druhú stranu, nevýhodou je rast 
koeficientov, ktorý pri väčších maticiach (alebo maticiach s veľkými/nesúdeliteľnými koeficientami) vedie k pretečeniu. 
V tomto prípade, je vypísaná chybová hláška - úloha prekročila možnosti tohto programu.

Program umožňuje vypísať "help" pomocou prepínačov `-h` alebo `--help`, prípadne zvoliť z jednej z dvoch implementácii:
1) Implementácia využívajúca jedno vlákno zavolaním `-o` alebo `--one-thread`. Jednotlivé zadania sú načítané do kontajneru
a následne jeden za druhým vyriešené a vypísané na výstup.


2) Implementácia využívajúca viac vláken (ak sú k dispozícii) zavolaním `-m` alebo `--multiple-threads`. Podobne ako pri 
jednovláknovom riešení sa najprv načítajú všetky problémy do kontajneru. Následne sa spustí hardware-om podporovaný 
počet súbežných pracovných vláken, ktoré si vyzobávajú problémy z kontejneru a zároveň priebežne vypisujú 
výsledky. Výstup je chránený zámkom, aby sa zabránilo premiešavaniu. Slabinou tohto prístupu je, že užívateľ 
nedostane výsledky v presne rovnakom poradí ako ich zadal. Identifikáciu je možné učiniť podľa vypísanej matice
(eventuálne by bolo možné pridať ID úlohy). Podľa testu, táto implementácia spočíta zadaný súbor rýchlejšie.

Iné prepínače alebo ich absencia vedú k chybovej hláške a vypísaniu help-u.

###Vzorové vstupné data:
1) Vstupné súbory `non-valid-input1.txt` a `non-valid-input2.txt` demonštruje správanie programu v prípade, že matice 
obsahujú neplatné vstupné koeficienty, či počet premenných. Program nie je robustný proti nesúhlasiacimi vstupnými 
údajmi - počet premenných v systéme a počet premenných zadaných vstupm. Ukážkový vstup je však vypísaný v help-e.
2) Vstupný súbor `all_versions.txt` demonštruje, ako vyzerajú množina riešení K vo všetkých troch prípadoch - žiadne 
riešenie, práve jedno riešenie, nekonečne veľa riešení.
3) Vstupný súbory `large.txt` poskytuje väčší testovací súbor (50 000 príkladov), pričom všetky tri typy možných 
výsledkov sú rovnomerne rozdelené. Súbor `large.txt` tiež poukazuje na limity tohto programu, ktoré sú pri takýchto 
koeficientoch (viď súbor) niekde okolo `n = 10`. Pozorujeme zopár sústav, pri ktorých už pretečie zásobník. Riešením by 
by mohla byť iný typ pre ukladanie koeficientu alebo dokonca konštrukcia vlastného typu s dynamicky alokovanou pamäťou
a vlastnou aritmetikou nad týmto typom. Prípadne, úplne iná architektúra. 

###Porovnanie implementácii:
Súbor `large.txt` som použil na porovnanie oboch implementácii s nasledujúcimi výsledkami:
Prebehlo 100 run-ov (binárka skompilovaná v Release móde)
1) v prípade jedno-vláknovej implementácie priemerne za **396ms +-6ms**;
2) v prípade viac-vláknovej implementácie priemerne za **245ms +-8ms**.

Testovanie prebehlo v prostredí:
- macOS Monterey Version 12.2
- Apple M1 chip:
  8-core CPU with 4 performance cores and 4 efficiency cores,
  8-core GPU,
  16-core Neural Engine