#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <fstream>
#include <deque>
#include <chrono>
#include <tuple>
#include <numeric>
#include <thread>
#include <atomic>
#include <ostream>
#include <sstream>
#include <mutex>
#include <unordered_set>
#include <string>


using Matrix = std::vector<std::vector<double>>;
using Problem = std::tuple<int, Matrix>;
using ProblemSet = std::deque<Problem>;


static std::mutex cout_mutex;

struct exclusive_ostringstream :
        public std::ostringstream {};

//  In multiple threads variant, we use guarding of std::cout with a lock to prevent mixing.
//  Solutions are then printed as-they-go.
std::ostream & operator<<(std::ostream & os, const exclusive_ostringstream & ss) {
    std::lock_guard<std::mutex> guard(cout_mutex);
    os << ss.str();
    return os;
}

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

// Auxiliary function for checking, whether input string is an integer.
bool is_integer(std::string & input)
{
    int iter=0;
    if (input.empty()) {
        return false;
    }
    if (input[0]=='-') {
        iter=1;
    }

    for (int i=iter;i<input.size();i++) {
        if (!std::isdigit(input[i]))
            return false;
    }
    return true;
}

void print_help() {
    std::cout << "Usage: Gauss [OPTION]\n"
                 "Options: \n"
                 "-h, --help               to print help manual\n"
                 "-o, --one-thread         Calculation using one thread only.\n"
                 "-m, --multiple-threads   Calculation using multiple threads, if available.\n\n"
                 "At the start of run user is asked to enter path with the input file.\n"
                 "\n"
                 "Example of an input:\n"
                 "\n"
                 "n\n"
                 "a11 a_12 ... a1n b1\n"
                 "a21 a_22 ... a2n b2\n"
                 " ... \n"
                 "an1 a_n2 ... ann bn, \n"
                 "\n"
                 "where n is the number of variables,\n"
                 "A=(aij) is matrix of the system and\n"
                 "b is vector of right hand sides.\n"
                 "All input coefficients have to be integers.\n";
}

void print(const Matrix & Ab, std::ostream & os) {
    int n = Ab.size();
    for (int i=0; i<n; i++) {
        for (int j=0; j<n+1; j++) {
            os << Ab[i][j] << "\t";
            if (j == n-1) {
                os << "| ";
            }
        }
        os << "\n";
    }
}

void swap_rows(Matrix & Ab, const int row1, const int row2) {
    std::swap(Ab[row1], Ab[row2]);
}

// Find maximal value under [row, col]. Used for determining, whether it is necessary to null values
// in the processed column during Gauss elimination.
double find_max_under(const Matrix & Ab, const int row, const int col) {
    int n = Ab.size();

    double max_value = std::abs(Ab[row][col]);
    for (int k=row+1; k<n; k++) {
        if (std::abs(Ab[k][col]) > max_value) {
            max_value = std::abs(Ab[k][col]);
        }
    }
    return max_value;
}


// This function is a part of the heuristic, that slow down the rise of the coefficients during Gauss elimination.
// It finds the best row for eliminating the others. The one that leads to the smallest portion of multiplication.
int find_best_under(const Matrix & Ab, const int row, const int col) {
    int n = Ab.size();

    long min_value = -1;
    int best_row = row;
    int cdiv;

    for (int j=row; j<n; j++) {
        long multiplicator = 1;
        for (int k=row; k<n; k++) {
            if ((j!=k) && (Ab[j][col]!=0) && (Ab[k][col]!=0)) {
                cdiv = std::gcd<long, long>(std::abs(Ab[j][col]),
                                            std::abs(Ab[k][col]));
                multiplicator *= std::abs(Ab[j][col]) / cdiv;
            }
        }
        if (min_value == -1 && (Ab[j][col]!=0)) {
            best_row = j;
            min_value = multiplicator;
        }
        else if (multiplicator < min_value  && (Ab[j][col]!=0)) {
            min_value = multiplicator;
            best_row = j;
        }

    }
    return best_row;
}

// Part of the Gauss elimination, that eliminates values (->0) under the currently
// processed position [upper_row, left_col] and provides success flag.
bool null_under(Matrix & Ab, const int upper_row, const int left_col) {
    int n = Ab.size();

    for (int k=upper_row+1; k<n; k++) {
        if ((Ab[k][left_col] != 0) && Ab[upper_row][left_col])
        {
//          Greatest common divisor is used for determining the smallest multiplicators we have to use
//          in order to null the required values. Obtaining zero indicates over-flows.
            int cdiv;
            cdiv = std::gcd<long, long>(std::abs(Ab[upper_row][left_col]),
                                        std::abs(Ab[k][left_col]));
            if (cdiv == 0) {return false;}
            double multiplicator1 = -Ab[k][left_col] / cdiv;
            double multiplicator2 = Ab[upper_row][left_col] / cdiv;
            for (int j = upper_row; j < n + 1; j++) {
                if (upper_row == j) {
                    Ab[k][j] = 0;
                } else {
                    Ab[k][j] *= multiplicator2;
                    Ab[k][j] += multiplicator1 * Ab[upper_row][j];
                }
            }
        }
    }
    return true;
}

// This function takes as an input upper triangular matrix and calculate its solution, just one. If the solution
// space has positive dimension, it still provides just one solution.
std::vector<double> provide_one_solution(const Matrix & upper, int dim, const std::vector<int> & pivots) {
    int n = upper.size();
    int m = n-dim;
    std::vector<double> line(m+1,0);
    Matrix temp(m, line);

    for (int col=0; col<m; col++) {
        for (int row=0; row<m; row++) {
            temp[row][col] = upper[row][pivots[col]];
        }
    }
    for (int row=0; row<m; row++) {
        temp[row][m] = upper[row][n];
    }


    std::vector<double> partial_solution(m);
    for (int i=m-1; i>=0; i--) {
        partial_solution[i] = temp[i][m] / temp[i][i];
        for (int k=i-1;k>=0; k--) {
            temp[k][m] -= temp[k][i] * partial_solution[i];
        }
    }
    std::vector<double> solution(n,0);
    for (int i=0; i< m; i++) { solution[pivots[i]] = partial_solution[i];}

    return solution;
}

// Checks whether system has solution or not according to Frobenius Theorem.
bool frobenius(const Matrix & Ab, int dim) {
    int n = Ab.size();

    for (int row=n-1; row > n-1-dim; row--) {
        if (Ab[row][n] != 0) {
            return false;
        }
    }

    return true;
}

// Given matrix describing system of linear equations, calculates upper triangular matrix using Gauss elimination.
// Result is required upper triangular matrix, pivots for future use and success flag that indicates too large
// coefficients.
std::tuple<Matrix, std::vector<int>, bool> get_upper(Matrix Ab) {
    int n = Ab.size();
    int cur_row = 0;
    std::vector<int> pivots;
    bool success = true;

    for (int cur_col=0; cur_col < n; cur_col++) {
        double max_value = find_max_under(Ab, cur_row, cur_col);
        if (max_value == 0 ) {
            continue;}

        int best_row = find_best_under(Ab, cur_row, cur_col);
        if (best_row!= cur_row) { swap_rows(Ab, cur_row, best_row);}

        success = success && null_under(Ab, cur_row, cur_col);
        pivots.push_back(cur_col);
        cur_row++;
    }

    return std::make_tuple(Ab, pivots, success);
}

// Calculates the basic vectors of the solution space.
Matrix find_base(const Matrix & upper, int dim, const std::vector<int> & pivots) {
    int n = upper.size();
    std::vector<double> line(n);
    Matrix base;
    Matrix temp;
    int col = 0;
    for (int row=0; row<dim; row++) {
        while (std::binary_search(pivots.begin(), pivots.end(), col)) {col++;}
        temp = upper;
        for (int row_=0; row_<n- dim; row_++) {temp[row_][n] = -upper[row_][col];}
        base.push_back(provide_one_solution(temp, dim,pivots));
        base[row][col] = 1;
        col++;
    }
    return base;
}

ProblemSet read_matrices(const std::string & path) {
    ProblemSet problems;
    bool bad_input;
    std::ifstream infile;
    infile.open(path);
    std::string temp;
    if (infile.is_open()) {
        int n;
        std::string temp_n;
        while (true) {

            if (!(infile >> temp_n)) break;
            if (is_integer(temp_n)) {
                n = std::stoi(temp_n);
                if (n<1) {
                    std::cout << "Input: \"" << temp_n << "\" IS NOT valid number of variables! It must be positive integer.\n";
                    break;
                }
            } else {
                std::cout << "Input: \"" << temp_n << "\" IS NOT valid number of variables! It must be positive integer.\n";
                break;
            }
            std::vector<double> line(n + 1, 0);
            Matrix Ab(n, line);
            bad_input = false;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n + 1; j++) {
                    infile >> temp;
                    if (is_integer(temp)) {
                        Ab[i][j] = std::stoi(temp);
                    } else {
                        bad_input = true;
                        std::cout << "Input: \"" << temp << "\" IS NOT valid input! It must be integer.\n";
                        break;
                    }
                }
            }
            if (bad_input) { break; }
            problems.emplace_back(n, Ab);
        }
        infile.close();
        return problems;
    } else {
        std::cout << "File " << path << " NOT found!\n";
        return problems;
    }
}

// Calculates result for given system of linear equations handing it to given output stream.
void solve_problem(int n, const Matrix & Ab, std::ostream & os) {
    print(Ab, os);

    Matrix upper;
    std::vector<int> pivots;
    bool success;
    std::tie(upper, pivots, success) = get_upper(Ab);

    int dimension;
    dimension = n - pivots.size();

    if (!success) {
        os << "This system of linear equations has reach the limits of this program. Try another. \n\n";
    }
    else if (!frobenius(upper, dimension)) {
        os << "There is no solution. \n\n";
    }
    else {
        std::vector<double> x(n);
        Matrix base;
        x = provide_one_solution(upper, dimension, pivots);

        if (dimension > 0) {
            base = find_base(upper, dimension, pivots);
        }

        os << "Result:\t";
        os << "K={[";
        for (int i = 0; i < n; i++) {
            if (i < n - 1) { os << x[i] << " "; }
            else { os << x[i] << "]"; }
        }
        if (dimension == 0) { os << "}"; }
        else {
            os << " + <";
            for (int s = 0; s < dimension; s++) {
                os << "[";
                for (int i = 0; i < n; i++) {
                    if (i < n - 1) { os << base[s][i] << " "; }
                    else { os << base[s][i] << "]"; }
                }
                if (s != dimension - 1) { os << ", "; }
            }
            os << ">";
        }
        os << "}\n\n";
    }
}

// Takes container of the problems and one by one solves it. Used for one-thread version only.
void solve_problems(ProblemSet & problems) {
    int n;
    Matrix Ab;
    std::tie(n, Ab) = problems.front();
    problems.pop_front();
    solve_problem(n, Ab, std::cout);
}

// Checks arguments given at command line.
bool in_options(const std::unordered_set<std::string> & args, const std::string & str) {
    return args.find(str) != args.end();
}

// Worker thread function which takes problems one-by-one using atomic work_idx till they are available,
//solves them and pushes output to the lock-guarded stringstream.
void worker(std::atomic<unsigned int> & work_idx, const ProblemSet & problems) {
    while (true) {
        const auto my_work = work_idx.fetch_add(1);

        if (my_work >= problems.size()) {break;}

        int n;
        Matrix Ab;
        std::tie(n, Ab) = problems[my_work];

        exclusive_ostringstream ss;
        solve_problem(n, Ab, ss);
        std::cout << ss;
    }
}


int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "ERROR! No OPTION is not valid. See help:\n\n";
        print_help();
        return 0;}

    else if (argc > 2){
        std::cout << "ERROR! Multiple OPTIONs are not valid. See help:\n\n";
        print_help();
        return 0;
    }
    else {
        std::unordered_set<std::string> arguments(argv + 1, argv + argc);
        if (in_options(arguments, "--help") || in_options(arguments, "-h")) {
            print_help();
            return 0;
        }

        else if (in_options(arguments, "--one-thread") || in_options(arguments, "-o")) {
            std::string path;
            std::cout << "Type path to the input file:\n";
            std::cin >> path;
            auto start = std::chrono::high_resolution_clock::now();
            ProblemSet problems = read_matrices(path);

            while (!problems.empty()) {solve_problems(problems);}
            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Calculation needed " << to_ms(end - start).count() << " ms to finish.\n";
            return 0;
        }

        else if (in_options(arguments, "--multiple-threads") || in_options(arguments, "-m")) {
            std::string path;
            std::cout << "Type path to the input file:\n";
            std::cin >> path;
            auto start = std::chrono::high_resolution_clock::now();

            const auto njobs = std::thread::hardware_concurrency();
            const ProblemSet problems = read_matrices(path);

            std::cout << "Will use " << njobs << " threads\n";
            std::atomic<unsigned int> work_idx = 0;

            std::vector<std::thread> workers;
            for (auto j = 0u; j < njobs; ++j) {
                workers.emplace_back(worker, std::ref(work_idx), std::cref(problems));
            }
            for (auto & t : workers) { t.join();}

            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Calculation needed " << to_ms(end - start).count() << " ms to finish.\n";
            return 0;
        }

        else  {
            std::cout << "ERROR! Unrecognized OPTION. See help:\n\n";
            print_help();
            return 0;
        }
    }
}
